YAMLLINT=@if which yamllint > /dev/null; then yamllint $@.yml; fi
ANSIBLELINT=@if which ansible-lint > /dev/null; then ansible-lint -q $@.yml; fi
PLAYBOOK=ansible-playbook $(OPTIONS) $@.yml 

all: KS_host KS_guests

install_roles:
	ansible-galaxy install -r requirements.yml

KS_host KS_guests: install_roles
	$(YAMLLINT)
	$(ANSIBLELINT)
	$(PLAYBOOK)
