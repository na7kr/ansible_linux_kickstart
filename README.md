ks
--

Playbooks to install and configure a PXE kickstart server on a CentOS 8 server

Requirements
------------

- Ansible provision server with make installed
- RHEL 8 compatible target server that will become the kickstart server *kshost*.
- DHCP server configured to use the the *kshost*
  If DHCP is already configured for normal use, you probably only have to add something like :
  - next-server: < ip of the *kshost* >;
  - boot-file-name: "pxelinux.0";
  Tested with KEA DHCP server
- NFS server with ISO files that can be NFS mounted from the *kshost*

Run Script
-------

make ( do not run .yml files)
the make file does all the work.

Refs
----

- https://www.golinuxcloud.com/configure-kickstart-pxe-boot-server-linux/ RHEL8/CentOS8
- https://linuxhint.com/tftp_server_centos8/

constants
---------

defined as vars in the playbook, but these are defaults of the tftp-server and ftp-server packages and should not be changed.
constants.yml


```
hostname_fqdn: "{{ ansible_hostname }}.domain.com"
timezone: America/Los_Angeles

ksguests:
   - name: Redhat8_5NFS
     iso: Unix/Linux/Redhat/8.5/rhel-8.5-x86_64-boot.iso
     pxebootdir: images/pxeboot
     type: "nfs"
     menu: "Redhat ^8.5 with KS file via NFS"
   #########################################################
   - name: Redhat8_5HTTPS
     iso: Unix/Linux/Redhat/8.5/rhel-8.5-x86_64-boot.iso
     pxebootdir: images/pxeboot
     type: "https"
     menu: "Redhat ^8.5 with KS file via HTTPS"
   #########################################################
   - name: Redhat8_5HTTP
     iso: Unix/Linux/Redhat/8.5/rhel-8.5-x86_64-boot.iso
     pxebootdir: images/pxeboot
     type: "http"
     menu: "Redhat ^8.5 with KS file via HTTP"
   #########################################################
   - name: Redhat8_5FTP
     iso: Unix/Linux/Redhat/8.5/rhel-8.5-x86_64-boot.iso
     pxebootdir: images/pxeboot
     type: "ftp"
     menu: "Redhat ^8.5 with KS file via FTP"
   #########################################################
```

As FTP, NFS and HTTP(S) use the same directory to share the distribution its referred to as distribution directory.


--------

Example with ISO's containing the currently latest stable versions of the OS to be kickstarted. All demo ISO's are FOSS. Download links can be found on for example https://distrowatch.com/


in a minimal deployment for testing the the *kshost* itself, the *ksguests* and *myksguests* variables can be an emtpy list.
```
 ksguests: []
 myksguests: []
```

So the private configuration is separated from the public. That way the generic part containing the public stuff can be stored
in a public repo while the private configuration can be maintained in a private repo.

How it works (network kickstart)
--------------------------------

1. kickstart client boots and PXE BIOS broadcasts DHCP request (DHCP)
2. DHCP server sends client IP address/subnet/nameserver info + the IP of the kickstart server with TFTP server (DHCP)
3. kickstart client requests kickstart server to provide kernel + boot image + kickstart file (TFTP)
4. kickstart clients boots received kernel with image and continues with what is configured in the received kickstart file (FTP/HTTP/HTTPS)/NFS)

How it works (this implementation)
----------------------------------

On the *kshost* a TFTP server, FTP server, HTTP(S) server and NFS server is installed and configured.
systemd is used for TFTP.
This is done in the *KS_host.yml* playbook.

In the second playbook *KS_guests* the *kshost* is prepared for the specific clients that can be kickstarted. 
The ISO files with the OS to install during kickstart are not locally uploaded.
Instead they will be loop-back mounted from the NFS server.
The idea is that there is a dedictated storage server containing the ISOs.
The ISO content becomes available in */var/ftp/pub* which is not only the root dir for FTP, but also configured as root dir for HTTP(S) and NFS.
After the ISO is loop-back mounted on */var/ftp/pub/{{ ksguests[].name}}.cdrom*
the pxeboot directory (as defined in hostvar *ksguests[].pxebootdir*) is copied to */var/lib/tftpboot/images/{{ ksguests[].name }}*. These files are needed for the tftp boot phase and symlinks to the ISO content did not work for TFTP during tests.
The rest of the OS data should be available from the ISO itself, served via FTP, HTTP(S) and NFS. For the kickstart clients the ISO content is available with following URLs :
```
- ftp://{{ hostname_fqdn }}/{{ ksguests[].name }}
- http://{{ hostname_fqdn }}/{{ ksguests[].name }}
- https://{{ hostname_fqdn }}/{{ ksguests[].name }}
- nfs://{{ hostname_fqdn }}/var/ftp/pub/{{ ksguests[].name }}
```
So the content from the ISO that was mounted in from external NFS storage server is served by the *kshost* via NFS too. This requires *crossmnt* export option.


For testing purposes the *ksguests* var can be initialized as empty list *[]*.
The DOS Image should be able to boot, it does not need any ISO. It will boot in RAM and will not install anything on the guest disk.
